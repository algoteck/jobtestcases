const data = [
  { name: "Bob", age: 32, gender: "male", student: false },
  { name: "Alice", age: 30, gender: "female", student: false },
  { name: "Philip", age: 22, gender: "male", student: true },
  { name: "Sam", age: 24, gender: "male", student: true },
  { name: "Anna", age: 28, gender: "female", student: false },
];

// ask 1: build a new array with age in months
// e.g [{ name: "Bob", age: 384, gender: "male" }, ...]
const task1 = data.map(({ name, age, gender }) => {
  return { name, age: age * 12, gender };
});

task1.forEach((x) => console.log(x));

// Task 2: find name of youngest person
const task2 = data.reduce((min, curr) => {
  return min.age < curr.age ? min : curr;
}, {});
console.log("Youngest person name: " + task2.name);

// Task 3: compute average age
const task3 = data.reduce((acc, curr) => {
  return acc + curr.age;
}, 0);
console.log("Average age: " + task3 / data.length);

// Task 4: concatenate names to comma separated string
// e.g "Bob,Alice,Philip,Sam,Anna"
const task4 = data.reduce((str, curr) => {
  if (str === "") {
    str = curr.name;
  } else {
    str += ", " + curr.name;
  }
  return str;
}, "");

console.log("Concatinated names: ", task4);
